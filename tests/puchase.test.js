import { expect } from 'chai';

import checkout from '../src/checkout';
import mockRequest from './fixtures/request';

// set pricing rule
process.env.PRICING_RULE = 'specialoffer';

describe('/checkout', () => {
  it('first scenario', async () => {
    const response = await checkout({
      body: JSON.stringify(mockRequest.scenario1),
    });

    expect(response.statusCode).equals(200);

    const responseBody = JSON.parse(response.body);
    expect(responseBody.total).equal('94.70');
  });

  it('second scenario', async () => {
    const response = await checkout({
      body: JSON.stringify(mockRequest.scenario2),
    });

    expect(response.statusCode).equals(200);

    const responseBody = JSON.parse(response.body);
    expect(responseBody.total).equal('209.40');
  });

  it('third scenario', async () => {
    const response = await checkout({
      body: JSON.stringify(mockRequest.scenario3),
    });

    expect(response.statusCode).equals(200);

    const responseBody = JSON.parse(response.body);
    expect(responseBody.total).equal('84.70');
    expect(responseBody.items).to.deep.include({
      code: '1gb',
      quantity: 2,
    });
  });

  it('fourth scenario', async () => {
    const response = await checkout({
      body: JSON.stringify(mockRequest.scenario4),
    });

    expect(response.statusCode).equals(200);

    const responseBody = JSON.parse(response.body);
    expect(responseBody.total).equal('31.32');
  });
});

describe('/checkout Invalid scenarios', () => {
  it('invalid promotion date', async () => {
    const response = await checkout({
      body: JSON.stringify(mockRequest.failPromoDate),
    });

    expect(response.statusCode).equals(200);

    const responseBody = JSON.parse(response.body);
    expect(responseBody.total).equal('119.60');
  });

  it('invalid pricing rule', async () => {
    process.env.PRICING_RULE = 'INVALID_PRICING_RULE';

    const response = await checkout({
      body: JSON.stringify(mockRequest.scenario1),
    });

    expect(response.statusCode).equals(500);
  });
});
