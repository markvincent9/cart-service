const VALID_PROMO_DATE = Date.parse('1/1/2019, 12:00:00');
const INVALID_PROMO_DATE = Date.parse('2/1/2019, 12:00:00');

export default {
  scenario1: {
    items: [
      {
        code: 'ult_small',
        quantity: 3,
      },
      {
        code: 'ult_large',
        quantity: 1,
      },
    ],
    timestamp: VALID_PROMO_DATE,
  },
  scenario2: {
    items: [
      {
        code: 'ult_small',
        quantity: 2,
      },
      {
        code: 'ult_large',
        quantity: 4,
      },
    ],
    timestamp: VALID_PROMO_DATE,
  },
  scenario3: {
    items: [
      {
        code: 'ult_small',
        quantity: 1,
      },
      {
        code: 'ult_medium',
        quantity: 2,
      },
    ],
    timestamp: VALID_PROMO_DATE,
  },
  scenario4: {
    items: [
      {
        code: 'ult_small',
        quantity: 1,
      },
      {
        code: '1gb',
        quantity: 1,
      },
    ],
    promoCode: 'I<3AMAYSIM',
    timestamp: VALID_PROMO_DATE,
  },
  failPromoDate: {
    items: [
      {
        code: 'ult_small',
        quantity: 3,
      },
      {
        code: 'ult_large',
        quantity: 1,
      },
    ],
    timestamp: INVALID_PROMO_DATE,
  },
};
