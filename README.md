# Cart Service

This service is responsible for the computation of total price of items added in the shopping cart.

## Prerequisites

1. Install NVM

   https://github.com/creationix/nvm

2. Install node.js version 8.10 (AWS Lambda latest supported version)

       nvm install 8.10

3. Register and install Serverless framework globally

       npm install -g serverless
   
4. Install Dependencies. Go to Project Directory

       npm install
      

#### Test End-points Locally

In order to test and debug end-points on your local machine, run

    sls offline start

##### Run Tests

    npm test

##### CURL test scripts

###### Scenario 1
```
curl -X POST http://localhost:3000/checkout -d '{"items": [{"code": "ult_small","quantity": 3},{"code": "ult_large","quantity": 1}],"timestamp": 1546315200000}'
```

###### Scenario 2
```
curl -X POST http://localhost:3000/checkout -d '{"items": [{"code": "ult_small","quantity": 2},{"code": "ult_large","quantity": 4}],"timestamp": 1546315200000}'
```

###### Scenario 3
```
curl -X POST http://localhost:3000/checkout -d '{"items": [{"code": "ult_small","quantity": 1},{"code": "ult_medium","quantity": 2}],"timestamp": 1546315200000}'
```

###### Scenario 4
```
curl -X POST http://localhost:3000/checkout -d '{"items": [{"code": "ult_small","quantity": 1},{"code": "1gb","quantity": 1}],"promoCode": "I<3AMAYSIM", "timestamp": 1546315200000}'
```