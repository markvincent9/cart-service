import _checkout from './src/checkout';

export const checkout = async event => _checkout(event);
