import PricingFactory from './pricing';
import products from './products';

export default class Cart {
  constructor(pricingRule) {
    this.pricing = PricingFactory(pricingRule);
    this.items = [];
    this.total = 0;
    this.timestamp = '';
  }

  addItem(item) {
    if (!item.code) throw new Error('Product code missing');
    if (!item.quantity) throw new Error('Quantity is missing of one of the items');
    if (!products[item.code]) throw new Error(`Invalid Product Code: ${item.code} `);

    this.items.push(item);
  }

  setPromoCode(promoCode) {
    this.promoCode = promoCode;
  }

  setTimeStamp(timestamp) {
    this.timestamp = timestamp;
  }

  checkout() {
    const cart = {
      items: this.items,
      promoCode: this.promoCode,
      timestamp: this.timestamp,
    };

    this.total = this.pricing.calculateTotal(cart);

    this.items = this.items.concat(this.pricing.getFreebies(cart));

    return {
      total: this.total.toFixed(2),
      items: this.items,
    };
  }
}
