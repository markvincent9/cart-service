import Cart from './cart';

export default event => {
  let response = {};

  try {
    const cart = new Cart(process.env.PRICING_RULE);

    const request = JSON.parse(event.body);
    const {
      items = [], promoCode, timestamp,
    } = request;

    if (!items.length || !timestamp) {
      return {
        statusCode: 400,
        body: JSON.stringify({
          error: 'Bad Request, Please send required fields.',
        }),
      };
    }

    if (promoCode) cart.setPromoCode(promoCode);
    cart.setTimeStamp(timestamp);

    items.forEach(item => {
      cart.addItem(item);
    });

    const checkout = cart.checkout();

    response = {
      statusCode: 200,
      body: JSON.stringify(checkout),
    };
  } catch (e) {
    response = {
      statusCode: 500,
      body: JSON.stringify({
        error: e.message,
      }),
    };
  }

  return response;
};
