import AbstractRule from './AbstractRule';

// Jan 1 to Jan 31 2019
const PROMO_START_DATE = Date.parse('1/1/2019, 00:00:01');
const PROMO_END_DATE = Date.parse('1/31/2019, 12:59:59');

export default class SpecialOfferRule extends AbstractRule {
  checkPromo(cart) {
    const { timestamp } = cart;
    if (timestamp >= PROMO_START_DATE && timestamp <= PROMO_END_DATE) {
      this.canAvailPromo = true;
    }
  }

  getDiscountedPrice(productCode, itemPrice, quantity) {
    const defaultValue = itemPrice * quantity;

    if (this.canAvailPromo) {
      switch (productCode) {
        case 'ult_small':
          if (quantity > 2) {
            const discountedItems = Math.ceil(quantity / 3);
            return defaultValue - (discountedItems * itemPrice);
          }
          return defaultValue;
        case 'ult_large':
          if (quantity > 3) {
            return defaultValue - (5 * quantity);
          }
          return defaultValue;
        default:
          return defaultValue;
      }
    } else {
      return defaultValue;
    }
  }

  getFreebie(item) {
    if (item.code === 'ult_medium' && this.canAvailPromo) {
      return {
        code: '1gb',
        quantity: item.quantity,
      };
    }
    return null;
  }

  applyPromoCode(promoCode, total) {
    let discount = 0;
    switch (promoCode) {
      case 'I<3AMAYSIM':
        discount = ((total * 10) / 100);
        break;
      default:
        throw new Error(`Invalid Promocode: ${promoCode}`);
    }

    if (this.canAvailPromo) return total - discount;
    return total;
  }
}
