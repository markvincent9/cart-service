import AbstractRule from './AbstractRule';

export default class StandardRule extends AbstractRule {
  getDiscountedPrice(productCode, itemPrice, quantity) {
    const defaultValue = itemPrice * quantity;

    return defaultValue;
  }

  // eslint-disable-next-line
  getFreebie(item) {
    return null;
  }

  applyPromoCode(promoCode, total) {
    return total;
  }
}
