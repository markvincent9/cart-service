import PRODUCTS from '../products';

export default class AbstractRule {
  constructor() {
    this.canAvailPromo = false;
  }

  // eslint-disable-next-line
  getDiscountedPrice(productCode, itemPrice, quantity, timestamp) {
    throw new Error('Method getDiscountedPrice Not Implemented');
  }

  // eslint-disable-next-line
  getFreebie(item) {
    throw new Error('Method getFreebie Not Implemented');
  }

  // eslint-disable-next-line
  applyPromoCode(promoCode) {
    throw new Error('Method applyPromoCode Not Implemented');
  }

  // eslint-disable-next-line
  checkPromo(cart) {
  }

  calculateTotal(cart) {
    const { items = [], promoCode } = cart;

    this.checkPromo(cart);

    let total = 0;
    items.forEach(item => {
      const { code, quantity } = item;
      const { price } = PRODUCTS[code];
      total += this.getDiscountedPrice(code, price, quantity);
    });

    if (promoCode) total = this.applyPromoCode(promoCode, total);

    return total;
  }

  getFreebies(cart) {
    const { items = [] } = cart;
    const freebies = [];

    this.checkPromo(cart);

    items.forEach(item => {
      const freebie = this.getFreebie(item);
      if (freebie) freebies.push(freebie);
    });

    return freebies;
  }
}
