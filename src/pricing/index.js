import StandardRule from './StandardRule';
import SpecialOfferRule from './SpecialOfferRule';

export default pricingRule => {
  switch (pricingRule) {
    case 'specialoffer':
      return new SpecialOfferRule();
    case 'standard':
      return new StandardRule();
    // other pricing rule
    default:
      throw new Error('Pricing Rule Not Found');
  }
};
